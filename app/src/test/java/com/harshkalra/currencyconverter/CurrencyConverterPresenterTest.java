package com.harshkalra.currencyconverter;

import com.harshkalra.currencyconverter.models.fixer.LatestRatesResponse;
import com.harshkalra.currencyconverter.models.fixer.Rates;
import com.harshkalra.currencyconverter.net.FixerApi;
import com.harshkalra.currencyconverter.ui.main.CurrencyConverterContract;
import com.harshkalra.currencyconverter.ui.main.CurrencyConverterPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by harshk on 11/17/16.
 */

public class CurrencyConverterPresenterTest {

    @Mock
    private CurrencyConverterContract.View view;

    @Mock
    private Call<LatestRatesResponse> mockCall;

    @Mock
    private FixerApi mockFixerApi;

    @Mock
    ResponseBody errorResponseBody;

    @Captor
    private ArgumentCaptor<Callback<LatestRatesResponse>> callbackCapture;

    private CurrencyConverterPresenter presenter;

    @Before
    public void setupPresenter() {
        MockitoAnnotations.initMocks(this);
        presenter = new CurrencyConverterPresenter(view, mockFixerApi);
    }

    @Test
    public void loadCurrenciesFromFixerApiAndLoadIntoView() {
        when( mockFixerApi.getLatestQuotes("USD")).thenReturn(mockCall);
        Response<LatestRatesResponse> rsp = Response.success(getFakeLatestRatesResponse());

        presenter.getLatestCurrencyRates("1");
        verify(mockCall).enqueue(callbackCapture.capture());
        callbackCapture.getValue().onResponse(null, rsp);
        verify(view).enableButton(true);
        verify(view).showCurrencyRatesRv(true);
    }

    @Test
    public void testApiUnsuccessful() {
        when( mockFixerApi.getLatestQuotes("USD")).thenReturn(mockCall);
        Response<LatestRatesResponse> rsp = Response.error(500, errorResponseBody);

        presenter.getLatestCurrencyRates("1");
        verify(mockCall).enqueue(callbackCapture.capture());
        callbackCapture.getValue().onResponse(null, rsp);
        verify(view).enableButton(true);
        verify(view).showError("An exception occurred while trying to get currencies!");
        verify(view, never()).showCurrencyRatesRv(true);
    }

    @Test
    public void testApiException() {
        when( mockFixerApi.getLatestQuotes("USD")).thenReturn(mockCall);

        presenter.getLatestCurrencyRates("1");
        verify(mockCall).enqueue(callbackCapture.capture());
        callbackCapture.getValue().onFailure(null, new IOException());
        verify(view).enableButton(true);
        verify(view).showError("An exception occurred while trying to get currencies!");
        verify(view, never()).showCurrencyRatesRv(true);
    }

    private LatestRatesResponse getFakeLatestRatesResponse() {
        LatestRatesResponse rsp = new LatestRatesResponse();
        rsp.setDate("2016-11-16");
        rsp.setBase("USD");
        rsp.setRates(new Rates(5.14d, 3d, 4d, 6d));
        return rsp;

    }


}