package com.harshkalra.currencyconverter;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by harshk on 11/14/16.
 */

public class WhatAHarshApp extends Application {

    private static WhatAHarshApp instance;
    private static Retrofit retrofitFixer;
    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static WhatAHarshApp getInstance() {
        return instance;
    }
}
