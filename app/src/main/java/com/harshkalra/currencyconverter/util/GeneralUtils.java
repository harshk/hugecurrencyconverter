package com.harshkalra.currencyconverter.util;

import android.app.Activity;

/**
 * Created by harshk on 11/19/16.
 */

public class GeneralUtils {

    public static boolean isActivityAlive(Activity activity) {
        if (activity == null || activity.isFinishing() || activity.isDestroyed()) {
            return false;
        }
        return true;
    }

}
