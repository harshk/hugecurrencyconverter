package com.harshkalra.currencyconverter.ui.main;

import com.harshkalra.currencyconverter.models.fixer.LatestRatesResponse;
import com.harshkalra.currencyconverter.models.fixer.CurrencyRate;
import com.harshkalra.currencyconverter.net.ApiProvider;
import com.harshkalra.currencyconverter.net.FixerApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harshk on 11/14/16.
 */

public class CurrencyConverterPresenter implements CurrencyConverterContract.ActionsListener {

    private CurrencyConverterContract.View contract;

    // in a real app, FixerApithis would be injected via dagger/DI
    private FixerApi fixerApi;

    public CurrencyConverterPresenter(CurrencyConverterContract.View contract, FixerApi fixerApi) {
        this.contract = contract;
        this.fixerApi = fixerApi;
    }

    @Override
    public void getLatestCurrencyRates(String numDollarsAsString) {
        try {
            Integer.valueOf(numDollarsAsString);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            contract.showError("Invalid integer");
            return;
        }

        int numDollars = Integer.valueOf(numDollarsAsString);;

        contract.enableButton(false);
        contract.showCurrenciesRatesRvEmptyView(true);
        contract.setCurrenciesRatesRvEmptyViewMsg("Loading Currencies .....");

        fixerApi.getLatestQuotes("USD").enqueue(new Callback<LatestRatesResponse>() {
            @Override
            public void onResponse(Call<LatestRatesResponse> call, Response<LatestRatesResponse> response) {
                if (response.isSuccessful()) {
                    contract.enableButton(true);
                    processResponse(response.body(), numDollars);
                } else {
                    handleError();
                }
            }

            @Override
            public void onFailure(Call<LatestRatesResponse> call, Throwable t) {
                t.printStackTrace();
                handleError();
            }
        });
    }

    private void handleError() {
        contract.enableButton(true);
        contract.showError("An exception occurred while trying to get currencies!");
    }

    private void processResponse(LatestRatesResponse response, int numDollars) {
        if (response == null || response.getRates() == null) {
            contract.showError("response == null || response.getRates() == null");
            return;
        }

        List<CurrencyRate> currencyRates = new ArrayList<>();
        currencyRates.add(new CurrencyRate("GBP", response.getRates().getGBP() * numDollars));
        currencyRates.add(new CurrencyRate("EUR", response.getRates().getEUR() * numDollars));
        currencyRates.add(new CurrencyRate("JPY", response.getRates().getJPY() * numDollars));
        currencyRates.add(new CurrencyRate("BRL", response.getRates().getBRL() * numDollars));

        contract.showCurrencyRates(currencyRates);
        contract.showCurrenciesRatesRvEmptyView(false);
        contract.showCurrencyRatesRv(true);
    }
}
