package com.harshkalra.currencyconverter.ui.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by harshk on 11/14/16.
 */

public class SelectableFontTextView extends AppCompatTextView {

    private static final String TAG = SelectableFontTextView.class.getSimpleName();
    private FontSetter f = new FontSetter();

    public SelectableFontTextView(Context context) {
        super(context);
    }

    public SelectableFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        f.setCustomFont(context, attrs, this);
    }

    public SelectableFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        f.setCustomFont(context, attrs, this);
    }


}