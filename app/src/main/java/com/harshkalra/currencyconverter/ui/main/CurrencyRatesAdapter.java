package com.harshkalra.currencyconverter.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static com.google.common.base.Preconditions.checkNotNull;
import com.harshkalra.currencyconverter.R;
import com.harshkalra.currencyconverter.models.fixer.CurrencyRate;
import com.harshkalra.currencyconverter.ui.currencyrategraph.CurrencyRateGraphActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshk on 11/15/16.
 */

public class CurrencyRatesAdapter extends RecyclerView.Adapter<CurrencyRatesAdapter.ViewHolder> {

    public static final String TAG = CurrencyRatesAdapter.class.getSimpleName();

    private Context context;
    private List<CurrencyRate> items;

    public CurrencyRatesAdapter(Context context, @NonNull List<CurrencyRate> items) {
        this.context = context;
        this.items = items;
    }

    public void clear() {
        items.clear();
    }

    public void setItems(@NonNull List<CurrencyRate> data) {
        checkNotNull(data);
        items.clear();
        items.addAll(data);
    }

    public void addItems(@NonNull List<CurrencyRate> data) {
        checkNotNull(data);
        items.addAll(data);
    }

    @Override
    public CurrencyRatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency, parent, false));
    }

    @Override
    public void onBindViewHolder(CurrencyRatesAdapter.ViewHolder holder, int position) {

        final CurrencyRate item = items.get(position);
        if (item == null || holder == null) {
            return;
        }

        holder.cardView.setOnClickListener(v -> CurrencyRateGraphActivity.start(context, holder.country.getText().toString()));

        if (!TextUtils.isEmpty(item.getCountry())) {
            holder.country.setText(item.getCountry());
        } else {
            holder.country.setText("");
        }

        if (!TextUtils.isEmpty(item.getCountry())) {
            holder.currencyRate.setText(String.valueOf(item.getCurrencyRate()));
        } else {
            holder.currencyRate.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.item_rate_card_view)
        CardView cardView;

        @Bind(R.id.item_currency_country)
        TextView country;

        @Bind(R.id.item_currency_rate)
        TextView currencyRate;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}