package com.harshkalra.currencyconverter.ui.main;

import android.support.annotation.NonNull;

import com.harshkalra.currencyconverter.models.fixer.CurrencyRate;

import java.util.List;

/**
 * Created by harshk on 11/17/16.
 */

public interface CurrencyConverterContract {

    interface View {
        void showCurrencyRates(@NonNull List<CurrencyRate> currencyRates);
        void enableButton(boolean enable);
        void showCurrencyRatesRv(boolean show);
        void showCurrenciesRatesRvEmptyView(boolean show);
        void setCurrenciesRatesRvEmptyViewMsg(String msg);
        void showError(String message);
    }

    interface ActionsListener {
        void getLatestCurrencyRates(String numDollarsAsString);
    }

}
