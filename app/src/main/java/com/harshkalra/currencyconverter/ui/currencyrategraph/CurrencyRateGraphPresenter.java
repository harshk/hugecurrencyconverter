package com.harshkalra.currencyconverter.ui.currencyrategraph;

import com.harshkalra.currencyconverter.models.fixer.LatestRatesResponse;
import com.harshkalra.currencyconverter.net.ApiProvider;
import com.harshkalra.currencyconverter.net.FixerApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by harshk on 11/16/16.
 */

public class CurrencyRateGraphPresenter implements CuurencyRateGraphActivityContract.ActionsListener {

    private static final String TAG = CurrencyRateGraphPresenter.class.getSimpleName();

    private CurrencyRateGraphActivity activity;

    private AtomicInteger requestCounter = new AtomicInteger(0);
    private int numDaysToGetRatesFor = 7;
    private List<LatestRatesResponse> responses = new ArrayList<>();
    private FixerApi fixerApi;

    public CurrencyRateGraphPresenter(CurrencyRateGraphActivity activity, FixerApi fixerApi) {
        this.activity = activity;
        this.fixerApi = fixerApi;
    }

    @Override
    public void getHistoricalCurrencies(String currency) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
        List<String> dates = new ArrayList<>();
        for (int i = 0; i < numDaysToGetRatesFor; i++) {
            dates.add(df.format(cal.getTime()));
            cal.add(Calendar.DATE, -1);
        }

        requestCounter.set(0);
        for (String date : dates) {
            getCurrencyForDate(date, currency);
        }
    }

    private void getCurrencyForDate(String date, String currency) {
        Observable<LatestRatesResponse> getLatestQuotes = fixerApi.getQuotesForDateObservable(date, "USD");
        getLatestQuotes
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(rsp -> {
                    requestCounter.incrementAndGet();
                    responses.add(rsp);
                    if (requestCounter.get() == numDaysToGetRatesFor) {
                        processFinalResponse(currency);
                    }
                },  exc -> {
                    requestCounter.incrementAndGet();
                    exc.printStackTrace();
                });
    }

    private void processFinalResponse(String currency) {

        Collections.sort(responses, new LatestRatesResponseComparator());

        for (LatestRatesResponse response : responses) {
            double rate = 0;
            switch (currency) {
                case "GBP" :
                    rate = response.getRates().getGBP();
                    break;
                case "EUR" :
                    rate = response.getRates().getEUR();
                    break;
                case "JPY" :
                    rate = response.getRates().getJPY();
                    break;
                case "BRL" :
                    rate = response.getRates().getBRL();
                    break;
            }
            activity.addDataPoint((float) rate);
        }
    }


    class LatestRatesResponseComparator implements Comparator<LatestRatesResponse> {
        @Override
        public int compare(LatestRatesResponse one, LatestRatesResponse two) {
            return one.getDate().compareTo(two.getDate());
        }
    }
}
