package com.harshkalra.currencyconverter.ui.currencyrategraph;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.common.collect.Lists;
import com.harshkalra.currencyconverter.R;
import com.harshkalra.currencyconverter.net.ApiProvider;
import com.harshkalra.currencyconverter.net.FixerApi;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshk on 11/15/16.
 */

public class CurrencyRateGraphActivity extends AppCompatActivity {

    private static final String TAG = CurrencyRateGraphActivity.class.getSimpleName();
    private static final String EXTRA_CURRENCY_TYPE = "currencyType";

    @Bind(R.id.chart)
    BarChart chart;

    private int dataPointIndex = 0;
    private ArrayList<BarEntry> dataPoints = new ArrayList<>();
    private CuurencyRateGraphActivityContract.ActionsListener actionsListener;

    public static void start(Context context, String currencyType) {
        Intent starter = new Intent(context, CurrencyRateGraphActivity.class);
        starter.putExtra(EXTRA_CURRENCY_TYPE, currencyType);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_rate_graph);
        ButterKnife.bind(this);

        actionsListener = new CurrencyRateGraphPresenter(this, ApiProvider.getInstance().getFixerApi());
        setupChart();

        if (getIntent().getStringExtra(EXTRA_CURRENCY_TYPE) != null) {
            actionsListener.getHistoricalCurrencies(getIntent().getStringExtra(EXTRA_CURRENCY_TYPE));
        }

    }

    private void setupChart() {

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
    }

    public void addDataPoint(float value) {

        dataPoints.add(new BarEntry(dataPointIndex++, value));

        BarDataSet set1 = new BarDataSet(dataPoints, getIntent().getStringExtra(EXTRA_CURRENCY_TYPE) + " : Past 7 Days");
        set1.setValues(dataPoints);
        set1.setColors(ContextCompat.getColor(this, R.color.hugeMagenta));

        ArrayList<IBarDataSet> dataSets = Lists.newArrayList(set1);
        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.9f);

        chart.setData(data);
        chart.notifyDataSetChanged();
        chart.invalidate();
    }

}
