package com.harshkalra.currencyconverter.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.harshkalra.currencyconverter.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by harshk on 11/14/16.
 */

public class FontSetter {

    private static final Map<String, Typeface> sTypefaceCache = new HashMap<>();

    public void setCustomFont(Context ctx, AttributeSet attrs, TextView view) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.SelectableFontWidget);
        String customFont = a.getString(R.styleable.SelectableFontWidget_customFont);
        a.recycle();

        if (customFont != null && !view.isInEditMode()) {
            view.setTypeface(getCachedTypeface(ctx, customFont));
        }
    }


    public static Typeface getCachedTypeface(Context context, String font) {
        synchronized (sTypefaceCache) {
            if (!sTypefaceCache.containsKey(font)) {
                Typeface tf = Typeface.createFromAsset(context.getApplicationContext().getAssets(), "fonts/" + font);
                sTypefaceCache.put(font, tf);
            }
            return sTypefaceCache.get(font);
        }
    }

}
