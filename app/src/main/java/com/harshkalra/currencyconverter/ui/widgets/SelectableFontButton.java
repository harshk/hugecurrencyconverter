package com.harshkalra.currencyconverter.ui.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by harshk on 11/14/16.
 */

public class SelectableFontButton extends AppCompatButton {

    private static final String TAG = SelectableFontButton.class.getSimpleName();
    private FontSetter f = new FontSetter();

    public SelectableFontButton(Context context) {
        super(context);
    }

    public SelectableFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        f.setCustomFont(context, attrs, this);
    }

    public SelectableFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        f.setCustomFont(context, attrs, this);
    }


}