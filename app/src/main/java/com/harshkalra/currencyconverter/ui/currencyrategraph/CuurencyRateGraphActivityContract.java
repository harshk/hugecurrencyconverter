package com.harshkalra.currencyconverter.ui.currencyrategraph;

/**
 * Created by harshk on 11/20/16.
 */

public interface CuurencyRateGraphActivityContract {

    interface View {

    }

    interface ActionsListener {
        void getHistoricalCurrencies(String currency);
    }

}
