package com.harshkalra.currencyconverter.ui.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by harshk on 11/14/16.
 */

public class SelectableFontEditText extends AppCompatEditText {

    private static final String TAG = SelectableFontEditText.class.getSimpleName();
    private FontSetter f = new FontSetter();

    public SelectableFontEditText(Context context) {
        super(context);
    }

    public SelectableFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        f.setCustomFont(context, attrs, this);
    }

    public SelectableFontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        f.setCustomFont(context, attrs, this);
    }


}