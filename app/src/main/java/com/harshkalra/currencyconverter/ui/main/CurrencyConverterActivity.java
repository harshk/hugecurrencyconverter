package com.harshkalra.currencyconverter.ui.main;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.google.common.base.Preconditions.checkNotNull;
import com.harshkalra.currencyconverter.R;
import com.harshkalra.currencyconverter.models.fixer.CurrencyRate;
import com.harshkalra.currencyconverter.net.ApiProvider;
import com.harshkalra.currencyconverter.util.GeneralUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CurrencyConverterActivity extends AppCompatActivity implements CurrencyConverterContract.View {

    private final static String STATE_RATES = "currencyRates";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.dollars_et)
    EditText tv;

    @Bind(R.id.btn)
    Button btn;

    @Bind(R.id.currencies_rv)
    RecyclerView currenciesRv;

    @Bind(R.id.currencies_rv_empty_view)
    TextView currenciesRvEmptyView;

    private CurrencyRatesAdapter adapter;
    private List<CurrencyRate> currencyRates;

    private CurrencyConverterContract.ActionsListener actionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        actionListener = new CurrencyConverterPresenter(this, ApiProvider.getInstance().getFixerApi());
        currencyRates = savedInstanceState != null && savedInstanceState.containsKey(STATE_RATES) ?
                savedInstanceState.getParcelableArrayList(STATE_RATES) :
                new ArrayList<>();
        initRecyclerView(currencyRates);

        btn.setOnClickListener(v -> {
            if (tv.getText().length() > 0) {
                actionListener.getLatestCurrencyRates(tv.getText().toString());
            } else {
                showError(getString(R.string.please_enter_value));
            }
        });

        currenciesRvEmptyView.setText(getString(R.string.main_activity_instructions));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (currencyRates != null) {
            savedInstanceState.putParcelableArrayList(STATE_RATES, new ArrayList<>(currencyRates));
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    private void initRecyclerView(@NonNull List<CurrencyRate> currencyRates) {
        checkNotNull(currencyRates);
        LinearLayoutManager layoutMgr = new LinearLayoutManager(this);
        layoutMgr.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new CurrencyRatesAdapter(this, currencyRates);
        currenciesRv.setLayoutManager(layoutMgr);
        currenciesRv.setAdapter(adapter);
    }

    @Override
    public void showCurrencyRates(@NonNull List<CurrencyRate> currencyRates) {
        checkNotNull(currencyRates);
        this.currencyRates = currencyRates;

        adapter.clear();
        adapter.setItems(currencyRates);
        adapter.notifyDataSetChanged();
        currenciesRv.setVisibility(View.VISIBLE);
    }

    @Override
    public void enableButton(boolean enable) {
        if (!GeneralUtils.isActivityAlive(this)) {
            return;
        }
        btn.setEnabled(enable);
    }

    @Override
    public void showCurrencyRatesRv(boolean show) {
        if (!GeneralUtils.isActivityAlive(this)) {
            return;
        }
        currenciesRv.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showCurrenciesRatesRvEmptyView(boolean show) {
        if (!GeneralUtils.isActivityAlive(this)) {
            return;
        }
        currenciesRvEmptyView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setCurrenciesRatesRvEmptyViewMsg(String msg) {
        if (!GeneralUtils.isActivityAlive(this)) {
            return;
        }
        currenciesRvEmptyView.setText(msg);
    }

    @Override
    public void showError(String message) {
        if (!GeneralUtils.isActivityAlive(this)) {
            return;
        }
        btn.setEnabled(true);
        Snackbar.make(toolbar, message, Snackbar.LENGTH_LONG).show();
    }

}
