package com.harshkalra.currencyconverter.net;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by harshk on 11/16/16.
 */

/***
 * In a real application, we would use a real DI framework, such as Dagger
 */
public class ApiProvider {

    private static ApiProvider instance;
    private OkHttpClient okhttpClient;
    private FixerApi fixerApi;

    public static ApiProvider getInstance() {
        if (instance == null) {
            instance = new ApiProvider();
        }
        return instance;
    }

    private ApiProvider() {

        okhttpClient = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io/")
                .client(okhttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        fixerApi = retrofit.create(FixerApi.class);
    }

    public FixerApi getFixerApi() {
        return fixerApi;
    }

    public OkHttpClient getOkHttpClient() {
        return okhttpClient;
    }

}
