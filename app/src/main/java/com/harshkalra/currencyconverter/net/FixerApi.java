package com.harshkalra.currencyconverter.net;

import com.harshkalra.currencyconverter.models.fixer.LatestRatesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by harshk on 11/14/16.
 */

public interface FixerApi {

    @GET("/latest")
    Call<LatestRatesResponse> getLatestQuotes(@Query("base") String baseCurrency);

    @GET("/latest")
    Observable<LatestRatesResponse> getLatestQuotesObservable(@Query("base") String baseCurrency);

    @GET("/{date}")
    Call<LatestRatesResponse> getQuotesForDate(@Path("date") String date, @Query("base") String baseCurrency);

    @GET("/{date}")
    Observable<LatestRatesResponse> getQuotesForDateObservable(@Path("date") String date, @Query("base") String baseCurrency);

}
