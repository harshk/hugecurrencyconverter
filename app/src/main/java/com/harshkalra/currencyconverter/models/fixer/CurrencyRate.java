package com.harshkalra.currencyconverter.models.fixer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by harshk on 11/15/16.
 */

public class CurrencyRate implements Parcelable {

    private String country;
    private Double currencyRate;

    public CurrencyRate() {
    }

    public CurrencyRate(String country, Double currencyRate) {
        this.country = country;
        this.currencyRate = currencyRate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    protected CurrencyRate(Parcel in) {
        country = in.readString();
        currencyRate = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country);
        if (currencyRate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(currencyRate);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CurrencyRate> CREATOR = new Parcelable.Creator<CurrencyRate>() {
        @Override
        public CurrencyRate createFromParcel(Parcel in) {
            return new CurrencyRate(in);
        }

        @Override
        public CurrencyRate[] newArray(int size) {
            return new CurrencyRate[size];
        }
    };
}