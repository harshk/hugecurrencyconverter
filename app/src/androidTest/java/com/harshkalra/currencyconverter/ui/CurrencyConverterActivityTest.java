package com.harshkalra.currencyconverter.ui;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.harshkalra.currencyconverter.R;
import com.harshkalra.currencyconverter.net.ApiProvider;
import com.harshkalra.currencyconverter.ui.main.CurrencyConverterActivity;
import com.jakewharton.espresso.OkHttp3IdlingResource;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class CurrencyConverterActivityTest {

    @Rule
    public ActivityTestRule<CurrencyConverterActivity> mActivityTestRule = new ActivityTestRule<>(CurrencyConverterActivity.class);

    @Test
    public void checkThatErrorSnackbarAppears() {
        onView(allOf(withId(R.id.currencies_rv_empty_view), withText(R.string.main_activity_instructions)))
                .check(matches(isDisplayed()));
        onView(withId(R.id.btn)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.please_enter_value)))
                .check(matches(isDisplayed()));
    }

    @Test
    public void checkLargeInteger() {
        onView(withId(R.id.dollars_et)).perform(replaceText("999999999999999999999"), closeSoftKeyboard());
        onView(withId(R.id.btn)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Invalid integer"))).check(matches(isDisplayed()));
    }

    @Test
    public void checkThatResultsAppear() {
        IdlingResource idlingResource = OkHttp3IdlingResource.create("okhttp", ApiProvider.getInstance().getOkHttpClient());
        Espresso.registerIdlingResources(idlingResource);

        onView(withId(R.id.dollars_et)).perform(replaceText("5"), closeSoftKeyboard());
        onView(withId(R.id.currencies_rv)).check(matches(not(isDisplayed())));
        onView(withId(R.id.btn)).perform(click());
        onView(withId(R.id.currencies_rv)).check(matches(isDisplayed()));

        Espresso.unregisterIdlingResources(idlingResource);
    }

}
