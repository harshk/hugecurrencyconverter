This is an Android app development exercise that I (Harsh) had to do for an interview with Huge Inc.

To run the app on your device or emulator, open up a terminal window and run the following command: 
`./gradlew installDebug`

To run JUnit tests, run the following command: 
`./gradlew testDebug`

To run instrumented UI tests: 
- Make sure you have a device plugged into your computer or an emulator is running
- In Developer Settings, turn off all animations
- Run the following command: 
`./gradlew connectedAndroidTest`
